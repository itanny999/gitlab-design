window.__imported__ = window.__imported__ || {};
window.__imported__["ce-32411-mr-view@1x/layers.json.js"] = [
	{
		"objectId": "2743C077-9DDE-4C63-B001-AE64420345DE",
		"kind": "artboard",
		"name": "topbar",
		"originalName": "topbar",
		"maskFrame": null,
		"layerFrame": {
			"x": 0,
			"y": 0,
			"width": 1280,
			"height": 102
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "5F1B0DA8-05E8-40AD-A146-8C4F18A29FF1",
				"kind": "group",
				"name": "top",
				"originalName": "top",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 1280,
					"height": 102
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-top-nuyxqjbe.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 1280,
						"height": 102
					}
				},
				"children": []
			}
		]
	},
	{
		"objectId": "CCF2483A-357D-410C-9356-D16A6FBB2D44",
		"kind": "artboard",
		"name": "sidebar",
		"originalName": "sidebar",
		"maskFrame": null,
		"layerFrame": {
			"x": 2360,
			"y": 102,
			"width": 290,
			"height": 698
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "471FD56C-A5E5-44F8-94D3-19100632C8AD",
				"kind": "group",
				"name": "side",
				"originalName": "side",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 0,
					"width": 290,
					"height": 1872
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"image": {
					"path": "images/Layer-side-ndcxrkq1.png",
					"frame": {
						"x": 0,
						"y": 0,
						"width": 290,
						"height": 1872
					}
				},
				"children": [
					{
						"objectId": "A90239C6-C4DA-4A74-80C5-74930F899E26",
						"kind": "group",
						"name": "todo",
						"originalName": "todo",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 10,
							"width": 259,
							"height": 46
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-todo-qtkwmjm5.png",
							"frame": {
								"x": 18,
								"y": 10,
								"width": 259,
								"height": 46
							}
						},
						"children": [
							{
								"objectId": "C8949994-DC22-4621-82D3-C2C7CA9FCC7A",
								"kind": "group",
								"name": "grey_button_default",
								"originalName": "grey-button-default",
								"maskFrame": null,
								"layerFrame": {
									"x": 158,
									"y": 10,
									"width": 83,
									"height": 35
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-grey_button_default-qzg5ndk5.png",
									"frame": {
										"x": 158,
										"y": 10,
										"width": 83,
										"height": 35
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "8D558A8F-85F0-4E0C-8581-DAD021CF1812",
						"kind": "group",
						"name": "assignee",
						"originalName": "assignee",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 74,
							"width": 259,
							"height": 55
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-assignee-oeq1nthb.png",
							"frame": {
								"x": 18,
								"y": 74,
								"width": 259,
								"height": 55
							}
						},
						"children": []
					},
					{
						"objectId": "CEA62DE1-5A53-47A0-BC1B-07BDDEA533DE",
						"kind": "group",
						"name": "milestone",
						"originalName": "milestone",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 147,
							"width": 259,
							"height": 55
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-milestone-q0vbnjje.png",
							"frame": {
								"x": 18,
								"y": 147,
								"width": 259,
								"height": 55
							}
						},
						"children": []
					},
					{
						"objectId": "CB5441E2-976A-4039-B9B1-7BDB5FD5B9F3",
						"kind": "group",
						"name": "labels",
						"originalName": "labels",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 220,
							"width": 259,
							"height": 65
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-labels-q0i1ndqx.png",
							"frame": {
								"x": 18,
								"y": 220,
								"width": 259,
								"height": 65
							}
						},
						"children": [
							{
								"objectId": "D7AD1AA7-EC99-4301-82A2-5D4950AC8241",
								"kind": "group",
								"name": "label_frontend",
								"originalName": "label--frontend",
								"maskFrame": null,
								"layerFrame": {
									"x": 18,
									"y": 243,
									"width": 70,
									"height": 25
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-label_frontend-rddbrdfb.png",
									"frame": {
										"x": 18,
										"y": 243,
										"width": 70,
										"height": 25
									}
								},
								"children": []
							},
							{
								"objectId": "A32E7038-D706-4D33-AD6C-9406B017A5E9",
								"kind": "group",
								"name": "label_pick_into_stable",
								"originalName": "label--pick-into-stable",
								"maskFrame": null,
								"layerFrame": {
									"x": 96,
									"y": 243,
									"width": 104,
									"height": 25
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-label_pick_into_stable-qtmyrtcw.png",
									"frame": {
										"x": 96,
										"y": 243,
										"width": 104,
										"height": 25
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "754CD68E-D174-4264-AB8F-874608884C9D",
						"kind": "group",
						"name": "participants",
						"originalName": "participants",
						"maskFrame": null,
						"layerFrame": {
							"x": 18,
							"y": 303,
							"width": 259,
							"height": 64
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-participants-nzu0q0q2.png",
							"frame": {
								"x": 18,
								"y": 303,
								"width": 259,
								"height": 64
							}
						},
						"children": [
							{
								"objectId": "C60CFC13-5F91-45C9-AA41-25191A7C1FDC",
								"kind": "group",
								"name": "user_picture",
								"originalName": "user-picture",
								"maskFrame": {
									"x": 0,
									"y": 0,
									"width": 22,
									"height": 22
								},
								"layerFrame": {
									"x": 18,
									"y": 326,
									"width": 24,
									"height": 24
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-user_picture-qzywq0zd.png",
									"frame": {
										"x": 18,
										"y": 326,
										"width": 24,
										"height": 24
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "81FD6913-BA3F-4E53-9737-0A5708455F4D",
						"kind": "group",
						"name": "notifications",
						"originalName": "notifications",
						"maskFrame": null,
						"layerFrame": {
							"x": 10,
							"y": 377,
							"width": 270,
							"height": 94
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-notifications-odfgrdy5.png",
							"frame": {
								"x": 10,
								"y": 377,
								"width": 270,
								"height": 94
							}
						},
						"children": [
							{
								"objectId": "179DA012-E012-4D17-BB44-B70904A050D1",
								"kind": "group",
								"name": "button",
								"originalName": "button",
								"maskFrame": null,
								"layerFrame": {
									"x": 194,
									"y": 377,
									"width": 83,
									"height": 35
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-button-mtc5reew.png",
									"frame": {
										"x": 194,
										"y": 377,
										"width": 83,
										"height": 35
									}
								},
								"children": []
							}
						]
					}
				]
			}
		]
	},
	{
		"objectId": "959CB02B-1B8F-48DA-8CF1-2E660C1369B2",
		"kind": "artboard",
		"name": "MR",
		"originalName": "MR",
		"maskFrame": null,
		"layerFrame": {
			"x": 2760,
			"y": 102,
			"width": 990,
			"height": 999
		},
		"visible": true,
		"metadata": {},
		"backgroundColor": "rgba(255, 255, 255, 1)",
		"children": [
			{
				"objectId": "B03C716C-5E18-449C-A24F-7CDECB408B4B",
				"kind": "group",
				"name": "mrviewcontent",
				"originalName": "mrviewcontent",
				"maskFrame": null,
				"layerFrame": {
					"x": 0,
					"y": 4,
					"width": 991,
					"height": 1825
				},
				"visible": true,
				"metadata": {
					"opacity": 1
				},
				"children": [
					{
						"objectId": "FA04AA5E-32AF-4DC4-BA73-E2CA674A006A",
						"kind": "group",
						"name": "titlebar",
						"originalName": "titlebar",
						"maskFrame": null,
						"layerFrame": {
							"x": 15,
							"y": 4,
							"width": 962,
							"height": 52
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-titlebar-rkewnefb.png",
							"frame": {
								"x": 15,
								"y": 4,
								"width": 962,
								"height": 52
							}
						},
						"children": [
							{
								"objectId": "0174AEFA-E87F-4827-9C15-0FF13669DD7F",
								"kind": "group",
								"name": "meta",
								"originalName": "meta",
								"maskFrame": null,
								"layerFrame": {
									"x": 15,
									"y": 16,
									"width": 694,
									"height": 25
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-meta-mde3neff.png",
									"frame": {
										"x": 15,
										"y": 16,
										"width": 694,
										"height": 25
									}
								},
								"children": [
									{
										"objectId": "40267789-6A0C-47AF-B720-0BEF7CE262F4",
										"kind": "group",
										"name": "Group_19_Copy",
										"originalName": "Group 19 Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 15,
											"y": 15.0645161290322,
											"width": 232,
											"height": 25
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_19_Copy-ndaynjc3.png",
											"frame": {
												"x": 15,
												"y": 15.0645161290322,
												"width": 232,
												"height": 25
											}
										},
										"children": [
											{
												"objectId": "3903BE1A-9218-4B66-8C42-6EB6542C121A",
												"kind": "group",
												"name": "Group_10",
												"originalName": "Group 10",
												"maskFrame": null,
												"layerFrame": {
													"x": 23,
													"y": 16,
													"width": 224,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_10-mzkwm0jf.png",
													"frame": {
														"x": 23,
														"y": 16,
														"width": 224,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "71876CD7-6DFD-4DDA-BCFB-AAEC31BAC0B0",
														"kind": "group",
														"name": "Group_2",
														"originalName": "Group 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 23,
															"y": 22,
															"width": 51,
															"height": 14
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_2-nze4nzzd.png",
															"frame": {
																"x": 23,
																"y": 22,
																"width": 51,
																"height": 14
															}
														},
														"children": [
															{
																"objectId": "1D8246A2-B8BF-405E-BAB1-44CE37E95DE8",
																"kind": "group",
																"name": "Group_20_Copy",
																"originalName": "Group 20 Copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 130,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_20_Copy-muq4mjq2.png",
																	"frame": {
																		"x": 130,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															},
															{
																"objectId": "C0283C0D-6563-4417-AD3E-33A33B4E81EE",
																"kind": "group",
																"name": "Group_7_Copy_2",
																"originalName": "Group 7 Copy 2",
																"maskFrame": null,
																"layerFrame": {
																	"x": 128,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_7_Copy_2-qzayodnd.png",
																	"frame": {
																		"x": 128,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															}
														]
													}
												]
											},
											{
												"objectId": "9A281642-0DBC-4123-B568-FA789076A16D",
												"kind": "group",
												"name": "Group",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 192,
													"y": 20,
													"width": 35,
													"height": 16
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": []
											}
										]
									},
									{
										"objectId": "59E2F889-3681-4705-89A9-6D9E12ACCE78",
										"kind": "group",
										"name": "Group_19",
										"originalName": "Group 19",
										"maskFrame": null,
										"layerFrame": {
											"x": 15,
											"y": 15.0645161290322,
											"width": 233,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_19-ntlfmky4.png",
											"frame": {
												"x": 15,
												"y": 15.0645161290322,
												"width": 233,
												"height": 25
											}
										},
										"children": [
											{
												"objectId": "F6F40F50-D96A-422A-A818-A096A45C46F3",
												"kind": "text",
												"name": "ID",
												"originalName": "ID",
												"maskFrame": null,
												"layerFrame": {
													"x": 208,
													"y": 23,
													"width": 33,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "!5142",
													"css": [
														"/* !5142: */",
														"font-family: HelveticaNeue;",
														"font-size: 14px;",
														"color: rgba(0,0,0,0.55);",
														"letter-spacing: 0;"
													]
												},
												"image": {
													"path": "images/Layer-ID-rjzgndbg.png",
													"frame": {
														"x": 208,
														"y": 23,
														"width": 33,
														"height": 11
													}
												},
												"children": []
											},
											{
												"objectId": "E570377B-AE7F-4912-B264-371984E000F2",
												"kind": "text",
												"name": "Type",
												"originalName": "Type",
												"maskFrame": null,
												"layerFrame": {
													"x": 92,
													"y": 22,
													"width": 100,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "Merge Request",
													"css": [
														"/* Merge Request: */",
														"font-family: SourceSansPro-Semibold;",
														"font-size: 16px;",
														"color: #5C5C5C;"
													]
												},
												"image": {
													"path": "images/Layer-Type-rtu3mdm3.png",
													"frame": {
														"x": 92,
														"y": 22,
														"width": 100,
														"height": 15
													}
												},
												"children": []
											},
											{
												"objectId": "F4617812-2865-4146-A772-A9F7F3610D67",
												"kind": "group",
												"name": "Group_101",
												"originalName": "Group 10",
												"maskFrame": null,
												"layerFrame": {
													"x": 23,
													"y": 16,
													"width": 225,
													"height": 24
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_10-rjq2mtc4.png",
													"frame": {
														"x": 23,
														"y": 16,
														"width": 225,
														"height": 24
													}
												},
												"children": [
													{
														"objectId": "7A277DCD-3AEF-4C96-8467-2BD05B5E7C33",
														"kind": "group",
														"name": "Group_21",
														"originalName": "Group 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 23,
															"y": 22,
															"width": 51,
															"height": 14
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_2-n0eynzde.png",
															"frame": {
																"x": 23,
																"y": 22,
																"width": 51,
																"height": 14
															}
														},
														"children": [
															{
																"objectId": "82B016D2-0543-4732-A9B6-13619DE8D60E",
																"kind": "group",
																"name": "Group_20_Copy_2",
																"originalName": "Group 20 Copy 2",
																"maskFrame": null,
																"layerFrame": {
																	"x": 130,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_20_Copy_2-odjcmde2.png",
																	"frame": {
																		"x": 130,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															},
															{
																"objectId": "9EEA5BD7-5F5F-4299-91F1-8D29B6AB57C8",
																"kind": "group",
																"name": "Group_7_Copy",
																"originalName": "Group 7 Copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 128,
																	"y": 21,
																	"width": 14,
																	"height": 14
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_7_Copy-ouvfqtvc.png",
																	"frame": {
																		"x": 128,
																		"y": 21,
																		"width": 14,
																		"height": 14
																	}
																},
																"children": []
															}
														]
													}
												]
											},
											{
												"objectId": "5EAAF018-C2C8-40C5-B24D-642DF8B012AD",
												"kind": "group",
												"name": "Group1",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 192,
													"y": 20,
													"width": 35,
													"height": 16
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"children": []
											}
										]
									},
									{
										"objectId": "9ABC7B7B-41DC-4B34-9E21-20016491C23F",
										"kind": "group",
										"name": "label_merge",
										"originalName": "label-merge",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 15.0645161290322,
											"width": 68,
											"height": 25
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-label_merge-oufcqzdc.png",
											"frame": {
												"x": 16,
												"y": 15.0645161290322,
												"width": 68,
												"height": 25
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "874D2BC5-4077-4F0F-AAD7-3E5B91381EB7",
								"kind": "group",
								"name": "button_edit",
								"originalName": "button-edit",
								"maskFrame": null,
								"layerFrame": {
									"x": 927,
									"y": 10,
									"width": 47,
									"height": 34
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-button_edit-odc0rdjc.png",
									"frame": {
										"x": 927,
										"y": 10,
										"width": 47,
										"height": 34
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "B2F6AD52-4365-4F60-AB41-1385327DCFC0",
						"kind": "group",
						"name": "branches",
						"originalName": "branches",
						"maskFrame": null,
						"layerFrame": {
							"x": 0,
							"y": 178,
							"width": 979,
							"height": 464
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-branches-qjjgnkfe.png",
							"frame": {
								"x": 0,
								"y": 178,
								"width": 979,
								"height": 464
							}
						},
						"children": [
							{
								"objectId": "68FA6CC3-B7DB-4E90-A552-BABF6610E42B",
								"kind": "text",
								"name": "collapse_icon_focus",
								"originalName": "collapse_icon_focus",
								"maskFrame": null,
								"layerFrame": {
									"x": 489,
									"y": 260,
									"width": 13,
									"height": 19
								},
								"visible": false,
								"metadata": {
									"opacity": 1,
									"string": "",
									"css": [
										"/* : */",
										"font-family: FontAwesome;",
										"font-size: 19px;",
										"color: #0069BC;",
										"letter-spacing: 0;"
									]
								},
								"image": {
									"path": "images/Layer-collapse_icon_focus-njhgqtzd.png",
									"frame": {
										"x": 489,
										"y": 260,
										"width": 13,
										"height": 19
									}
								},
								"children": []
							},
							{
								"objectId": "10A87592-FB63-45A4-A574-DA2BDDC5D3C0",
								"kind": "text",
								"name": "collapse_icon",
								"originalName": "collapse_icon",
								"maskFrame": null,
								"layerFrame": {
									"x": 489,
									"y": 266,
									"width": 12,
									"height": 7
								},
								"visible": true,
								"metadata": {
									"opacity": 1,
									"string": "",
									"css": [
										"/* : */",
										"opacity: 0.6;",
										"font-family: FontAwesome;",
										"font-size: 19px;",
										"color: #000000;",
										"letter-spacing: 0;"
									]
								},
								"image": {
									"path": "images/Layer-collapse_icon-mtbbodc1.png",
									"frame": {
										"x": 489,
										"y": 266,
										"width": 12,
										"height": 7
									}
								},
								"children": []
							},
							{
								"objectId": "12166F43-D13B-4B7E-A0F7-2D67BF933819",
								"kind": "group",
								"name": "widget",
								"originalName": "widget",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 326,
									"width": 958,
									"height": 316
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "90DF37DE-248B-44EE-8A42-07F825BDB132",
										"kind": "group",
										"name": "body",
										"originalName": "body",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 385,
											"width": 958,
											"height": 242
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-body-otberjm3.png",
											"frame": {
												"x": 16,
												"y": 385,
												"width": 958,
												"height": 242
											}
										},
										"children": [
											{
												"objectId": "CCC524AE-BAFE-46ED-8B22-79E579790212",
												"kind": "group",
												"name": "stop_environment_button_copy_2",
												"originalName": "stop-environment-button copy 2",
												"maskFrame": null,
												"layerFrame": {
													"x": 574,
													"y": 499,
													"width": 99,
													"height": 25
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-stop_environment_button_copy_2-q0ndnti0.png",
													"frame": {
														"x": 574,
														"y": 499,
														"width": 99,
														"height": 25
													}
												},
												"children": []
											},
											{
												"objectId": "E608D8F5-9827-47C7-9A50-8A406E09BA04",
												"kind": "group",
												"name": "Group_23",
												"originalName": "Group 23",
												"maskFrame": null,
												"layerFrame": {
													"x": 509,
													"y": 499,
													"width": 55,
													"height": 25
												},
												"visible": true,
												"metadata": {
													"opacity": 0.35
												},
												"image": {
													"path": "images/Layer-Group_23-rtywoeq4.png",
													"frame": {
														"x": 509,
														"y": 499,
														"width": 55,
														"height": 25
													}
												},
												"children": []
											},
											{
												"objectId": "339FBD7C-D265-4629-9863-206639E36192",
												"kind": "text",
												"name": "command_line_instructions",
												"originalName": "command-line-instructions",
												"maskFrame": null,
												"layerFrame": {
													"x": 68,
													"y": 612,
													"width": 466,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1,
													"string": "You can also merge or inspect manually using the command line instructions",
													"css": [
														"/* You can also merge o: */",
														"font-family: SourceSansPro-It;",
														"font-size: 15px;",
														"color: #333333;",
														"letter-spacing: 0;",
														"line-height: 20px;"
													]
												},
												"image": {
													"path": "images/Layer-command_line_instructions-mzm5rkje.png",
													"frame": {
														"x": 68,
														"y": 612,
														"width": 466,
														"height": 15
													}
												},
												"children": []
											},
											{
												"objectId": "EABCE6B7-EFBE-46C1-BC44-48B2BD7E6E7A",
												"kind": "group",
												"name": "Group_9_Copy_5",
												"originalName": "Group 9 Copy 5",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 387,
													"width": 22,
													"height": 22
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy_5-rufcq0u2.png",
													"frame": {
														"x": 31,
														"y": 387,
														"width": 22,
														"height": 22
													}
												},
												"children": []
											},
											{
												"objectId": "5C6EB3D4-AAAC-48D6-BFAA-792F8CFCE587",
												"kind": "group",
												"name": "Group_9",
												"originalName": "Group 9",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 500,
													"width": 22,
													"height": 22
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9-num2ruiz.png",
													"frame": {
														"x": 31,
														"y": 500,
														"width": 22,
														"height": 22
													}
												},
												"children": []
											},
											{
												"objectId": "0E1389A8-83B9-4E20-A308-5116E535C20E",
												"kind": "group",
												"name": "Group_18",
												"originalName": "Group 18",
												"maskFrame": null,
												"layerFrame": {
													"x": 16,
													"y": 385,
													"width": 958,
													"height": 213
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_18-meuxmzg5.png",
													"frame": {
														"x": 16,
														"y": 385,
														"width": 958,
														"height": 213
													}
												},
												"children": [
													{
														"objectId": "E50FAA48-883B-44BB-B24B-B970A68ED323",
														"kind": "group",
														"name": "Group_211",
														"originalName": "Group 21",
														"maskFrame": null,
														"layerFrame": {
															"x": 31,
															"y": 385,
															"width": 387,
															"height": 27
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_21-rtuwrkfb.png",
															"frame": {
																"x": 31,
																"y": 385,
																"width": 387,
																"height": 27
															}
														},
														"children": [
															{
																"objectId": "3DBF3CE5-BD19-4B5D-9C96-3DBE38471971",
																"kind": "group",
																"name": "Group_9_Copy_3",
																"originalName": "Group 9 Copy 3",
																"maskFrame": null,
																"layerFrame": {
																	"x": 31,
																	"y": 387,
																	"width": 22,
																	"height": 22
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_9_Copy_3-m0rcrjnd.png",
																	"frame": {
																		"x": 31,
																		"y": 387,
																		"width": 22,
																		"height": 22
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "FF315A6A-4B90-4046-B5D8-0018D6E61370",
														"kind": "group",
														"name": "Group_20",
														"originalName": "Group 20",
														"maskFrame": null,
														"layerFrame": {
															"x": 17,
															"y": 417,
															"width": 956,
															"height": 38
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_20-rkyzmtvb.png",
															"frame": {
																"x": 17,
																"y": 417,
																"width": 956,
																"height": 38
															}
														},
														"children": [
															{
																"objectId": "E6EAC4CE-FB02-4152-A994-7D632FF4EAE1",
																"kind": "group",
																"name": "Group_22",
																"originalName": "Group 22",
																"maskFrame": null,
																"layerFrame": {
																	"x": 58,
																	"y": 426,
																	"width": 62,
																	"height": 22
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_22-rtzfqum0.png",
																	"frame": {
																		"x": 58,
																		"y": 426,
																		"width": 62,
																		"height": 22
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "96D12377-F6FC-40B2-A5CD-4F9360926F52",
														"kind": "group",
														"name": "MR_body_text",
														"originalName": "MR-body-text",
														"maskFrame": null,
														"layerFrame": {
															"x": 67,
															"y": 449,
															"width": 206,
															"height": 15
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-MR_body_text-otzemtiz.png",
															"frame": {
																"x": 67,
																"y": 449,
																"width": 206,
																"height": 15
															}
														},
														"children": [
															{
																"objectId": "870E80BC-36B3-4B78-B913-F2BBE5B2141C",
																"kind": "group",
																"name": "stop_environment_button_copy",
																"originalName": "stop-environment-button copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 448,
																	"y": 438,
																	"width": 130,
																	"height": 24
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-stop_environment_button_copy-odcwrtgw.png",
																	"frame": {
																		"x": 448,
																		"y": 438,
																		"width": 130,
																		"height": 24
																	}
																},
																"children": []
															},
															{
																"objectId": "912EA175-78B8-4D55-97C8-B33E1EEF9579",
																"kind": "group",
																"name": "environment_buttons",
																"originalName": "environment buttons",
																"maskFrame": null,
																"layerFrame": {
																	"x": 62,
																	"y": 470,
																	"width": 475,
																	"height": 127
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "0DABF68E-194E-41C1-9D84-C90740C3CAE8",
																		"kind": "group",
																		"name": "Group_14",
																		"originalName": "Group 14",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 481,
																			"y": 470,
																			"width": 56,
																			"height": 26
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"children": [
																			{
																				"objectId": "31C56B11-A7C6-4469-8F59-71CCD9EC5A7F",
																				"kind": "group",
																				"name": "stop_environment_button_copy_3",
																				"originalName": "stop-environment-button copy 3",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 511,
																					"y": 471,
																					"width": 26,
																					"height": 25
																				},
																				"visible": true,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-stop_environment_button_copy_3-mzfdntzc.png",
																					"frame": {
																						"x": 511,
																						"y": 471,
																						"width": 26,
																						"height": 25
																					}
																				},
																				"children": []
																			},
																			{
																				"objectId": "FA8FFAFF-A90E-45C6-B645-E9C8ED983B1B",
																				"kind": "group",
																				"name": "stop_environment_button_copy_4",
																				"originalName": "stop-environment-button copy 4",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 481,
																					"y": 471,
																					"width": 31,
																					"height": 25
																				},
																				"visible": true,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-stop_environment_button_copy_4-rke4rkzb.png",
																					"frame": {
																						"x": 481,
																						"y": 471,
																						"width": 31,
																						"height": 25
																					}
																				},
																				"children": []
																			}
																		]
																	},
																	{
																		"objectId": "E3BB66D7-9C89-4457-86C6-362E7637EEF2",
																		"kind": "group",
																		"name": "Group_13_Copy",
																		"originalName": "Group 13 Copy",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 62,
																			"y": 572,
																			"width": 399,
																			"height": 25
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"children": [
																			{
																				"objectId": "DF41BB68-F51A-4E96-B488-B625D23387A0",
																				"kind": "group",
																				"name": "stop_environment_button_copy_5",
																				"originalName": "stop-environment-button copy 5",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 62,
																					"y": 572,
																					"width": 159,
																					"height": 25
																				},
																				"visible": false,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-stop_environment_button_copy_5-rey0mujc.png",
																					"frame": {
																						"x": 62,
																						"y": 572,
																						"width": 159,
																						"height": 25
																					}
																				},
																				"children": []
																			},
																			{
																				"objectId": "9BB7BE71-E6FE-4802-B37A-EC04C9DDB2B8",
																				"kind": "group",
																				"name": "stop_environment_button_copy_21",
																				"originalName": "stop-environment-button copy 2",
																				"maskFrame": null,
																				"layerFrame": {
																					"x": 302,
																					"y": 572,
																					"width": 159,
																					"height": 25
																				},
																				"visible": false,
																				"metadata": {
																					"opacity": 1
																				},
																				"image": {
																					"path": "images/Layer-stop_environment_button_copy_2-oujcn0jf.png",
																					"frame": {
																						"x": 302,
																						"y": 572,
																						"width": 159,
																						"height": 25
																					}
																				},
																				"children": []
																			}
																		]
																	}
																]
															},
															{
																"objectId": "36797C3C-F5D6-4E9E-A91C-504B8246FEDA",
																"kind": "group",
																"name": "Group_13",
																"originalName": "Group 13",
																"maskFrame": null,
																"layerFrame": {
																	"x": 721,
																	"y": 384,
																	"width": 243,
																	"height": 30
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "7515A0BD-A916-4F1C-A01C-42C53BFA7091",
																		"kind": "group",
																		"name": "Group_6",
																		"originalName": "Group 6",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 721,
																			"y": 384,
																			"width": 243,
																			"height": 30
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-Group_6-nzuxnuew.png",
																			"frame": {
																				"x": 721,
																				"y": 384,
																				"width": 243,
																				"height": 30
																			}
																		},
																		"children": []
																	}
																]
															},
															{
																"objectId": "668EA90D-F688-4476-B7A1-4ABBA3BF2EFF",
																"kind": "group",
																"name": "Group_4",
																"originalName": "Group 4",
																"maskFrame": null,
																"layerFrame": {
																	"x": 477,
																	"y": 469,
																	"width": 30,
																	"height": 30
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_4-njy4rue5.png",
																	"frame": {
																		"x": 477,
																		"y": 469,
																		"width": 30,
																		"height": 30
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "A82F80D2-13BC-464F-95A1-D8B3A68395BE",
														"kind": "group",
														"name": "Group_9_Copy_2",
														"originalName": "Group 9 Copy 2",
														"maskFrame": null,
														"layerFrame": {
															"x": 37,
															"y": 508,
															"width": 10,
															"height": 11
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy_2-qtgyrjgw.png",
															"frame": {
																"x": 37,
																"y": 508,
																"width": 10,
																"height": 11
															}
														},
														"children": []
													},
													{
														"objectId": "30508B45-D363-474C-BA32-AFEC1DD1F3EB",
														"kind": "group",
														"name": "Group_9_Copy",
														"originalName": "Group 9 Copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 31,
															"y": 443,
															"width": 22,
															"height": 22
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy-mza1mdhc.png",
															"frame": {
																"x": 31,
																"y": 443,
																"width": 22,
																"height": 22
															}
														},
														"children": []
													}
												]
											}
										]
									},
									{
										"objectId": "81847B07-7A86-41B4-9AD1-AE2F3FB3CF91",
										"kind": "group",
										"name": "header",
										"originalName": "header",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 343,
											"width": 958,
											"height": 29
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-header-ode4nddc.png",
											"frame": {
												"x": 16,
												"y": 343,
												"width": 958,
												"height": 29
											}
										},
										"children": []
									},
									{
										"objectId": "DAE4EA20-E2D7-4572-9BBD-E8A918973E92",
										"kind": "group",
										"name": "container",
										"originalName": "container",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 326,
											"width": 958,
											"height": 316
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-container-reffnevb.png",
											"frame": {
												"x": 16,
												"y": 326,
												"width": 958,
												"height": 316
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "C52259AB-0043-4CD9-B22F-3E6A2671FDFE",
								"kind": "group",
								"name": "branch_id",
								"originalName": "branch-id",
								"maskFrame": null,
								"layerFrame": {
									"x": 15,
									"y": 328,
									"width": 317,
									"height": 25
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "2125A0B4-6422-4A1A-A969-9545A7A0B95F",
										"kind": "group",
										"name": "Group_27_Copy",
										"originalName": "Group 27 Copy",
										"maskFrame": null,
										"layerFrame": {
											"x": 248,
											"y": 329,
											"width": 84,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_27_Copy-mjeynuew.png",
											"frame": {
												"x": 248,
												"y": 329,
												"width": 84,
												"height": 24
											}
										},
										"children": []
									},
									{
										"objectId": "A350A763-7445-4E20-A472-C30FE9A7F810",
										"kind": "group",
										"name": "Group_27",
										"originalName": "Group 27",
										"maskFrame": null,
										"layerFrame": {
											"x": 15,
											"y": 329,
											"width": 221,
											"height": 24
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_27-qtm1mee3.png",
											"frame": {
												"x": 15,
												"y": 329,
												"width": 221,
												"height": 24
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "209DAC63-E013-4EA9-8471-1FBEB3411C1F",
								"kind": "group",
								"name": "divider_focus",
								"originalName": "divider_focus",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 286,
									"width": 958,
									"height": 1
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-divider_focus-mja5refd.png",
									"frame": {
										"x": 16,
										"y": 286,
										"width": 958,
										"height": 1
									}
								},
								"children": []
							},
							{
								"objectId": "1EDFFE5D-056D-4D57-B5CB-74044791F967",
								"kind": "group",
								"name": "divider",
								"originalName": "divider",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 286,
									"width": 958,
									"height": 1
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-divider-muverkzf.png",
									"frame": {
										"x": 16,
										"y": 286,
										"width": 958,
										"height": 1
									}
								},
								"children": []
							},
							{
								"objectId": "F03675BC-99F8-4368-B259-E3B2FF3EC775",
								"kind": "group",
								"name": "collapse_gradient_focus",
								"originalName": "collapse_gradient_focus",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 176,
									"width": 958,
									"height": 110
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-collapse_gradient_focus-rjaznjc1.png",
									"frame": {
										"x": 16,
										"y": 176,
										"width": 958,
										"height": 110
									}
								},
								"children": []
							},
							{
								"objectId": "5DC80DFF-D3A2-43DE-91A9-7EF1A9C65001",
								"kind": "group",
								"name": "collapse_gradient",
								"originalName": "collapse_gradient",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 178,
									"width": 958,
									"height": 108
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-collapse_gradient-nurdodbe.png",
									"frame": {
										"x": 16,
										"y": 178,
										"width": 958,
										"height": 108
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "8BD64A57-B117-43AC-BA06-1FA4021BFCCB",
						"kind": "group",
						"name": "tab_tabs",
						"originalName": "tab_tabs",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 702,
							"width": 958,
							"height": 51
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-tab_tabs-oejenjrb.png",
							"frame": {
								"x": 16,
								"y": 702,
								"width": 958,
								"height": 51
							}
						},
						"children": [
							{
								"objectId": "2A363CF3-C4EA-4537-8AC3-62E3054899AA",
								"kind": "group",
								"name": "unresolved_discussions",
								"originalName": "unresolved_discussions",
								"maskFrame": null,
								"layerFrame": {
									"x": 724,
									"y": 711,
									"width": 250,
									"height": 30
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-unresolved_discussions-mkeznjnd.jpg",
									"frame": {
										"x": 724,
										"y": 711,
										"width": 250,
										"height": 30
									}
								},
								"children": [
									{
										"objectId": "77564AE2-4E06-48CA-BD45-306CC7CA97D9",
										"kind": "group",
										"name": "Group_28",
										"originalName": "Group 28",
										"maskFrame": null,
										"layerFrame": {
											"x": 724,
											"y": 711,
											"width": 250,
											"height": 30
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_28-nzc1njrb.png",
											"frame": {
												"x": 724,
												"y": 711,
												"width": 250,
												"height": 30
											}
										},
										"children": [
											{
												"objectId": "3320BF26-D79F-4B37-BC1E-FF46374394A8",
												"kind": "group",
												"name": "Group_9_Copy_8",
												"originalName": "Group 9 Copy 8",
												"maskFrame": null,
												"layerFrame": {
													"x": 734,
													"y": 719,
													"width": 14,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy_8-mzmymejg.png",
													"frame": {
														"x": 734,
														"y": 719,
														"width": 14,
														"height": 15
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "8C6A865E-925C-49F0-A553-613021665D87",
								"kind": "group",
								"name": "discussion",
								"originalName": "discussion",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 702,
									"width": 111,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "CD44766D-CBB2-4BA7-8BAF-B80731893AA1",
										"kind": "text",
										"name": "title",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 27,
											"y": 720,
											"width": 65,
											"height": 12
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Discussion",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-q0q0ndc2.png",
											"frame": {
												"x": 27,
												"y": 720,
												"width": 65,
												"height": 12
											}
										},
										"children": []
									},
									{
										"objectId": "608E28FD-BD1E-4741-AB2C-039B0B34D9D3",
										"kind": "group",
										"name": "underline",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 750,
											"width": 111,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-nja4rti4.png",
											"frame": {
												"x": 16,
												"y": 750,
												"width": 111,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "98A4B7D5-D979-483E-8DF2-BBBE265B4887",
										"kind": "group",
										"name": "count_badge",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 96,
											"y": 718,
											"width": 23,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-othbnei3.png",
											"frame": {
												"x": 96,
												"y": 718,
												"width": 23,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "F2C7F091-60E5-4A4D-9BD8-B1B6CD27147D",
										"kind": "group",
										"name": "bg",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 702,
											"width": 111,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-rjjdn0yw.png",
											"frame": {
												"x": 16,
												"y": 702,
												"width": 111,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "8FB11E11-73D2-429C-9D95-2CF25FDAB707",
								"kind": "group",
								"name": "commits",
								"originalName": "commits",
								"maskFrame": null,
								"layerFrame": {
									"x": 127,
									"y": 702,
									"width": 101,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "BA420031-6737-4974-A912-D482B4894787",
										"kind": "text",
										"name": "title1",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 137,
											"y": 720,
											"width": 56,
											"height": 12
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Commits",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-qke0mjaw.png",
											"frame": {
												"x": 137,
												"y": 720,
												"width": 56,
												"height": 12
											}
										},
										"children": []
									},
									{
										"objectId": "29C3AF34-B2FB-4DB9-85A9-ACFFD72AFAF4",
										"kind": "group",
										"name": "underline1",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 127,
											"y": 750,
											"width": 101,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-mjldm0fg.png",
											"frame": {
												"x": 127,
												"y": 750,
												"width": 101,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "6363CD60-85D0-4482-9457-ADDD4E60E4CA",
										"kind": "group",
										"name": "count_badge1",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 197,
											"y": 718,
											"width": 21,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-njm2m0ne.png",
											"frame": {
												"x": 197,
												"y": 718,
												"width": 21,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "7F4ADE78-54C7-4C77-BB32-B4D26CF77BBD",
										"kind": "group",
										"name": "bg1",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 127,
											"y": 702,
											"width": 101,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-n0y0qurf.png",
											"frame": {
												"x": 127,
												"y": 702,
												"width": 101,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "7B240D8C-0984-4999-84A3-BC228F45BA91",
								"kind": "group",
								"name": "pipelines",
								"originalName": "pipelines",
								"maskFrame": null,
								"layerFrame": {
									"x": 228,
									"y": 702,
									"width": 102,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "22957DD4-786E-401B-9DE7-834F582E397B",
										"kind": "text",
										"name": "title2",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 239,
											"y": 720,
											"width": 56,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Pipelines",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-mji5ntde.png",
											"frame": {
												"x": 239,
												"y": 720,
												"width": 56,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "EE92D553-7170-4C2F-9CFB-49EE13680605",
										"kind": "group",
										"name": "underline2",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 228,
											"y": 750,
											"width": 102,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-ruu5mkq1.png",
											"frame": {
												"x": 228,
												"y": 750,
												"width": 102,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "E8AF02A1-C880-4530-A8D6-24622BE46C90",
										"kind": "group",
										"name": "count_badge2",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 299,
											"y": 718,
											"width": 21,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-rthbrjay.png",
											"frame": {
												"x": 299,
												"y": 718,
												"width": 21,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "B16F3A92-9B12-4B31-ABDF-3EEF0B2A14D4",
										"kind": "group",
										"name": "bg2",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 228,
											"y": 702,
											"width": 102,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-qje2rjnb.png",
											"frame": {
												"x": 228,
												"y": 702,
												"width": 102,
												"height": 50
											}
										},
										"children": []
									}
								]
							},
							{
								"objectId": "72B42CE8-013B-4D52-BD5F-EA41D4407F33",
								"kind": "group",
								"name": "changes",
								"originalName": "changes",
								"maskFrame": null,
								"layerFrame": {
									"x": 330,
									"y": 702,
									"width": 100,
									"height": 50
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"children": [
									{
										"objectId": "76576FE9-CF37-45F1-A7A5-AEA62215912B",
										"kind": "text",
										"name": "title3",
										"originalName": "title",
										"maskFrame": null,
										"layerFrame": {
											"x": 340,
											"y": 720,
											"width": 53,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "Changes",
											"css": [
												"/* title: */",
												"font-family: SourceSansPro-Regular;",
												"font-size: 15px;",
												"color: #000000;"
											]
										},
										"image": {
											"path": "images/Layer-title-nzy1nzzg.png",
											"frame": {
												"x": 340,
												"y": 720,
												"width": 53,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "8193088F-2F0E-45E0-8653-4FB14E53C822",
										"kind": "group",
										"name": "underline3",
										"originalName": "underline",
										"maskFrame": null,
										"layerFrame": {
											"x": 330,
											"y": 750,
											"width": 100,
											"height": 2
										},
										"visible": false,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-underline-ode5mza4.png",
											"frame": {
												"x": 330,
												"y": 750,
												"width": 100,
												"height": 2
											}
										},
										"children": []
									},
									{
										"objectId": "A08BBC86-A896-4090-BEF9-20D5B651ED0E",
										"kind": "group",
										"name": "count_badge3",
										"originalName": "count-badge",
										"maskFrame": null,
										"layerFrame": {
											"x": 397,
											"y": 718,
											"width": 21,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-count_badge-qta4qkjd.png",
											"frame": {
												"x": 397,
												"y": 718,
												"width": 21,
												"height": 19
											}
										},
										"children": []
									},
									{
										"objectId": "94AD1606-ECC7-4AC5-8997-09B92133535F",
										"kind": "group",
										"name": "bg3",
										"originalName": "bg",
										"maskFrame": null,
										"layerFrame": {
											"x": 330,
											"y": 702,
											"width": 100,
											"height": 50
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-bg-otrbrde2.png",
											"frame": {
												"x": 330,
												"y": 702,
												"width": 100,
												"height": 50
											}
										},
										"children": []
									}
								]
							}
						]
					},
					{
						"objectId": "F2380074-C20F-4D20-9697-BCD5B167A87A",
						"kind": "group",
						"name": "tab_content",
						"originalName": "tab_content",
						"maskFrame": null,
						"layerFrame": {
							"x": 12,
							"y": 753,
							"width": 979,
							"height": 1076
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-tab_content-rjizodaw.png",
							"frame": {
								"x": 12,
								"y": 753,
								"width": 979,
								"height": 1076
							}
						},
						"children": [
							{
								"objectId": "C5CE6423-4770-408C-9D16-E37B696EABA1",
								"kind": "group",
								"name": "discussion_tab",
								"originalName": "discussion_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 12,
									"y": 754,
									"width": 979,
									"height": 1075
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-discussion_tab-qzvdrty0.jpg",
									"frame": {
										"x": 12,
										"y": 754,
										"width": 979,
										"height": 1075
									}
								},
								"children": []
							},
							{
								"objectId": "879E1EF8-35EB-4907-8926-38A011DE560B",
								"kind": "group",
								"name": "commits_tab",
								"originalName": "commits_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 10,
									"y": 752,
									"width": 970,
									"height": 91
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-commits_tab-odc5rtff.jpg",
									"frame": {
										"x": 10,
										"y": 752,
										"width": 970,
										"height": 91
									}
								},
								"children": []
							},
							{
								"objectId": "0E55D9EE-EDE2-44E6-9B86-99F1C9D1E172",
								"kind": "group",
								"name": "pipelines_tab",
								"originalName": "pipelines_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 5,
									"y": 754,
									"width": 977,
									"height": 170
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-pipelines_tab-meu1nuq5.jpg",
									"frame": {
										"x": 5,
										"y": 754,
										"width": 977,
										"height": 170
									}
								},
								"children": []
							},
							{
								"objectId": "3C36E230-33E9-4CB8-8C72-6A64B9C7BA06",
								"kind": "group",
								"name": "changes_tab",
								"originalName": "changes_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": -16,
									"y": 751,
									"width": 1027,
									"height": 9120
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-changes_tab-m0mznkuy.jpg",
									"frame": {
										"x": -16,
										"y": 751,
										"width": 1027,
										"height": 9120
									}
								},
								"children": []
							},
							{
								"objectId": "38E61D6C-AA7D-40C7-ACE5-BEEC760F6B5C",
								"kind": "group",
								"name": "mergestatus_tab",
								"originalName": "mergestatus_tab",
								"maskFrame": null,
								"layerFrame": {
									"x": 15,
									"y": 753,
									"width": 960,
									"height": 329
								},
								"visible": false,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-mergestatus_tab-mzhfnjfe.png",
									"frame": {
										"x": 15,
										"y": 753,
										"width": 960,
										"height": 329
									}
								},
								"children": [
									{
										"objectId": "FF3DC947-F97C-4DE7-9E30-9D4C3C4A9A95",
										"kind": "group",
										"name": "stop_environment_button_copy_22",
										"originalName": "stop-environment-button copy 2",
										"maskFrame": null,
										"layerFrame": {
											"x": 574,
											"y": 882,
											"width": 99,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-stop_environment_button_copy_2-rkyzrem5.png",
											"frame": {
												"x": 574,
												"y": 882,
												"width": 99,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "0269D94D-2FE3-4AA8-844F-7CA649773CD2",
										"kind": "group",
										"name": "Group_231",
										"originalName": "Group 23",
										"maskFrame": null,
										"layerFrame": {
											"x": 509,
											"y": 882,
											"width": 55,
											"height": 25
										},
										"visible": true,
										"metadata": {
											"opacity": 0.35
										},
										"image": {
											"path": "images/Layer-Group_23-mdi2ouq5.png",
											"frame": {
												"x": 509,
												"y": 882,
												"width": 55,
												"height": 25
											}
										},
										"children": []
									},
									{
										"objectId": "4E23FE63-65AF-48E7-9326-32552FA22474",
										"kind": "text",
										"name": "command_line_instructions1",
										"originalName": "command-line-instructions",
										"maskFrame": null,
										"layerFrame": {
											"x": 68,
											"y": 995,
											"width": 466,
											"height": 15
										},
										"visible": true,
										"metadata": {
											"opacity": 1,
											"string": "You can also merge or inspect manually using the command line instructions",
											"css": [
												"/* You can also merge o: */",
												"font-family: SourceSansPro-It;",
												"font-size: 15px;",
												"color: #333333;",
												"letter-spacing: 0;",
												"line-height: 20px;"
											]
										},
										"image": {
											"path": "images/Layer-command_line_instructions-neuym0zf.png",
											"frame": {
												"x": 68,
												"y": 995,
												"width": 466,
												"height": 15
											}
										},
										"children": []
									},
									{
										"objectId": "DCFCA785-4258-4FA3-86E7-7C3C1CD60D95",
										"kind": "group",
										"name": "Group_9_Copy_51",
										"originalName": "Group 9 Copy 5",
										"maskFrame": null,
										"layerFrame": {
											"x": 31,
											"y": 770,
											"width": 22,
											"height": 22
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9_Copy_5-rengq0e3.png",
											"frame": {
												"x": 31,
												"y": 770,
												"width": 22,
												"height": 22
											}
										},
										"children": []
									},
									{
										"objectId": "6FA71784-011C-48A9-94B0-011A1EACC248",
										"kind": "group",
										"name": "Group_91",
										"originalName": "Group 9",
										"maskFrame": null,
										"layerFrame": {
											"x": 31,
											"y": 883,
											"width": 22,
											"height": 22
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_9-nkzbnze3.png",
											"frame": {
												"x": 31,
												"y": 883,
												"width": 22,
												"height": 22
											}
										},
										"children": []
									},
									{
										"objectId": "261527A1-8A55-4379-B88A-F85A6C65EB6E",
										"kind": "group",
										"name": "Group_181",
										"originalName": "Group 18",
										"maskFrame": null,
										"layerFrame": {
											"x": 16,
											"y": 768,
											"width": 958,
											"height": 213
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"image": {
											"path": "images/Layer-Group_18-mjyxnti3.png",
											"frame": {
												"x": 16,
												"y": 768,
												"width": 958,
												"height": 213
											}
										},
										"children": [
											{
												"objectId": "2F651224-992F-468D-9602-ED1E379C23A5",
												"kind": "group",
												"name": "Group_212",
												"originalName": "Group 21",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 768,
													"width": 387,
													"height": 27
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_21-mky2ntey.png",
													"frame": {
														"x": 31,
														"y": 768,
														"width": 387,
														"height": 27
													}
												},
												"children": [
													{
														"objectId": "AE067D22-7403-4DF1-81E5-7EC697EB958E",
														"kind": "group",
														"name": "Group_9_Copy_31",
														"originalName": "Group 9 Copy 3",
														"maskFrame": null,
														"layerFrame": {
															"x": 31,
															"y": 770,
															"width": 22,
															"height": 22
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_9_Copy_3-quuwnjde.png",
															"frame": {
																"x": 31,
																"y": 770,
																"width": 22,
																"height": 22
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "507FD3B4-466A-4A4D-A00F-BA72BE43FA8B",
												"kind": "group",
												"name": "Group_201",
												"originalName": "Group 20",
												"maskFrame": null,
												"layerFrame": {
													"x": 17,
													"y": 800,
													"width": 956,
													"height": 38
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_20-nta3rkqz.png",
													"frame": {
														"x": 17,
														"y": 800,
														"width": 956,
														"height": 38
													}
												},
												"children": [
													{
														"objectId": "D0D1D264-03E4-4EFD-8BA5-328848E858C0",
														"kind": "group",
														"name": "Group_221",
														"originalName": "Group 22",
														"maskFrame": null,
														"layerFrame": {
															"x": 58,
															"y": 809,
															"width": 62,
															"height": 22
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_22-rdbemuqy.png",
															"frame": {
																"x": 58,
																"y": 809,
																"width": 62,
																"height": 22
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "E348FCF5-220C-4D83-ABB6-7FFE5289EE0C",
												"kind": "group",
												"name": "MR_body_text1",
												"originalName": "MR-body-text",
												"maskFrame": null,
												"layerFrame": {
													"x": 67,
													"y": 832,
													"width": 206,
													"height": 15
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-MR_body_text-rtm0oezd.png",
													"frame": {
														"x": 67,
														"y": 832,
														"width": 206,
														"height": 15
													}
												},
												"children": [
													{
														"objectId": "3F3061A4-016B-46CB-939A-D43633B526A1",
														"kind": "group",
														"name": "stop_environment_button_copy1",
														"originalName": "stop-environment-button copy",
														"maskFrame": null,
														"layerFrame": {
															"x": 448,
															"y": 821,
															"width": 130,
															"height": 24
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-stop_environment_button_copy-m0yzmdyx.png",
															"frame": {
																"x": 448,
																"y": 821,
																"width": 130,
																"height": 24
															}
														},
														"children": []
													},
													{
														"objectId": "69AB4481-5ED4-497B-A1A6-2FDFE0AA96E4",
														"kind": "group",
														"name": "environment_buttons1",
														"originalName": "environment buttons",
														"maskFrame": null,
														"layerFrame": {
															"x": 62,
															"y": 853,
															"width": 475,
															"height": 127
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"children": [
															{
																"objectId": "C7355D7C-FEBB-49DA-9AFE-3D1F15A59B7C",
																"kind": "group",
																"name": "Group_141",
																"originalName": "Group 14",
																"maskFrame": null,
																"layerFrame": {
																	"x": 481,
																	"y": 853,
																	"width": 56,
																	"height": 26
																},
																"visible": false,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "C01C4B25-C8A3-4803-818F-A9DC770C5975",
																		"kind": "group",
																		"name": "stop_environment_button_copy_31",
																		"originalName": "stop-environment-button copy 3",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 511,
																			"y": 854,
																			"width": 26,
																			"height": 25
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_3-qzaxqzrc.png",
																			"frame": {
																				"x": 511,
																				"y": 854,
																				"width": 26,
																				"height": 25
																			}
																		},
																		"children": []
																	},
																	{
																		"objectId": "399EBD30-5483-40BC-9CE9-930E3E5E04C9",
																		"kind": "group",
																		"name": "stop_environment_button_copy_41",
																		"originalName": "stop-environment-button copy 4",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 481,
																			"y": 854,
																			"width": 31,
																			"height": 25
																		},
																		"visible": true,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_4-mzk5ruje.png",
																			"frame": {
																				"x": 481,
																				"y": 854,
																				"width": 31,
																				"height": 25
																			}
																		},
																		"children": []
																	}
																]
															},
															{
																"objectId": "D4A32F63-192D-4FAE-81D7-F30C3E78457A",
																"kind": "group",
																"name": "Group_13_Copy1",
																"originalName": "Group 13 Copy",
																"maskFrame": null,
																"layerFrame": {
																	"x": 62,
																	"y": 955,
																	"width": 399,
																	"height": 25
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"children": [
																	{
																		"objectId": "CFE638B6-8308-4B21-B18E-D2AF3A12D283",
																		"kind": "group",
																		"name": "stop_environment_button_copy_51",
																		"originalName": "stop-environment-button copy 5",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 62,
																			"y": 955,
																			"width": 159,
																			"height": 25
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_5-q0zfnjm4.png",
																			"frame": {
																				"x": 62,
																				"y": 955,
																				"width": 159,
																				"height": 25
																			}
																		},
																		"children": []
																	},
																	{
																		"objectId": "3CC83CE3-CA97-449B-B587-3A2792D94C35",
																		"kind": "group",
																		"name": "stop_environment_button_copy_23",
																		"originalName": "stop-environment-button copy 2",
																		"maskFrame": null,
																		"layerFrame": {
																			"x": 302,
																			"y": 955,
																			"width": 159,
																			"height": 25
																		},
																		"visible": false,
																		"metadata": {
																			"opacity": 1
																		},
																		"image": {
																			"path": "images/Layer-stop_environment_button_copy_2-m0ndodnd.png",
																			"frame": {
																				"x": 302,
																				"y": 955,
																				"width": 159,
																				"height": 25
																			}
																		},
																		"children": []
																	}
																]
															}
														]
													},
													{
														"objectId": "F72934D1-842D-48C5-B73A-05D8108B9360",
														"kind": "group",
														"name": "Group_131",
														"originalName": "Group 13",
														"maskFrame": null,
														"layerFrame": {
															"x": 721,
															"y": 767,
															"width": 243,
															"height": 30
														},
														"visible": true,
														"metadata": {
															"opacity": 1
														},
														"children": [
															{
																"objectId": "57A68D87-CDFB-4B5C-8886-6EA4786D9DCA",
																"kind": "group",
																"name": "Group_61",
																"originalName": "Group 6",
																"maskFrame": null,
																"layerFrame": {
																	"x": 721,
																	"y": 767,
																	"width": 243,
																	"height": 30
																},
																"visible": true,
																"metadata": {
																	"opacity": 1
																},
																"image": {
																	"path": "images/Layer-Group_6-ntdbnjhe.png",
																	"frame": {
																		"x": 721,
																		"y": 767,
																		"width": 243,
																		"height": 30
																	}
																},
																"children": []
															}
														]
													},
													{
														"objectId": "BF401B3B-7CEB-4F44-8E6A-2555F0F6CFBB",
														"kind": "group",
														"name": "Group_41",
														"originalName": "Group 4",
														"maskFrame": null,
														"layerFrame": {
															"x": 477,
															"y": 852,
															"width": 30,
															"height": 30
														},
														"visible": false,
														"metadata": {
															"opacity": 1
														},
														"image": {
															"path": "images/Layer-Group_4-qky0mdfc.png",
															"frame": {
																"x": 477,
																"y": 852,
																"width": 30,
																"height": 30
															}
														},
														"children": []
													}
												]
											},
											{
												"objectId": "14F270B6-FF44-47E7-858D-B4C119042337",
												"kind": "group",
												"name": "Group_9_Copy_21",
												"originalName": "Group 9 Copy 2",
												"maskFrame": null,
												"layerFrame": {
													"x": 37,
													"y": 891,
													"width": 10,
													"height": 11
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy_2-mtrgmjcw.png",
													"frame": {
														"x": 37,
														"y": 891,
														"width": 10,
														"height": 11
													}
												},
												"children": []
											},
											{
												"objectId": "5FCCCC37-FFE4-4A29-BCD3-1F51B04EECDC",
												"kind": "group",
												"name": "Group_9_Copy1",
												"originalName": "Group 9 Copy",
												"maskFrame": null,
												"layerFrame": {
													"x": 31,
													"y": 826,
													"width": 22,
													"height": 22
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group_9_Copy-nuzdq0nd.png",
													"frame": {
														"x": 31,
														"y": 826,
														"width": 22,
														"height": 22
													}
												},
												"children": []
											}
										]
									}
								]
							}
						]
					},
					{
						"objectId": "5CDCFC4C-7EDC-45A6-9678-2EA43C6650FA",
						"kind": "group",
						"name": "award_emoji",
						"originalName": "award_emoji",
						"maskFrame": null,
						"layerFrame": {
							"x": 16,
							"y": 640,
							"width": 958,
							"height": 62
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-award_emoji-nuneq0zd.png",
							"frame": {
								"x": 16,
								"y": 640,
								"width": 958,
								"height": 62
							}
						},
						"children": [
							{
								"objectId": "CE2E1BD0-6E97-4B63-BF7C-A5BE77478715",
								"kind": "group",
								"name": "thumbs_down_button",
								"originalName": "thumbs-down-button",
								"maskFrame": null,
								"layerFrame": {
									"x": 72,
									"y": 654,
									"width": 48,
									"height": 33
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-thumbs_down_button-q0uyrtfc.png",
									"frame": {
										"x": 72,
										"y": 654,
										"width": 48,
										"height": 33
									}
								},
								"children": [
									{
										"objectId": "BA3DE485-D1C9-4631-8AF3-684D42C78231",
										"kind": "group",
										"name": "$1f44e",
										"originalName": "1f44e",
										"maskFrame": null,
										"layerFrame": {
											"x": 82,
											"y": 662,
											"width": 14,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "B9DD6AED-54F0-4C2C-A89E-B9C19F59825A",
												"kind": "group",
												"name": "Group2",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 82.5,
													"y": 662,
													"width": 13,
													"height": 19
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group-qjlerdzb.png",
													"frame": {
														"x": 82.5,
														"y": 662,
														"width": 13,
														"height": 19
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "6C82D466-89F8-4B77-8CF5-72A952354EFD",
								"kind": "group",
								"name": "thumbs_up_button",
								"originalName": "thumbs-up-button",
								"maskFrame": null,
								"layerFrame": {
									"x": 16,
									"y": 654,
									"width": 47,
									"height": 33
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-thumbs_up_button-nkm4mkq0.png",
									"frame": {
										"x": 16,
										"y": 654,
										"width": 47,
										"height": 33
									}
								},
								"children": [
									{
										"objectId": "6FC9EAF8-431C-40D5-84AC-3FAF3B8AB964",
										"kind": "group",
										"name": "$1f44d",
										"originalName": "1f44d",
										"maskFrame": null,
										"layerFrame": {
											"x": 26,
											"y": 662,
											"width": 13,
											"height": 19
										},
										"visible": true,
										"metadata": {
											"opacity": 1
										},
										"children": [
											{
												"objectId": "7A04EB78-2D05-4874-8F5F-AE18F2239C27",
												"kind": "group",
												"name": "Group3",
												"originalName": "Group",
												"maskFrame": null,
												"layerFrame": {
													"x": 26.5,
													"y": 662,
													"width": 13,
													"height": 19
												},
												"visible": true,
												"metadata": {
													"opacity": 1
												},
												"image": {
													"path": "images/Layer-Group-n0ewnevc.png",
													"frame": {
														"x": 26.5,
														"y": 662,
														"width": 13,
														"height": 19
													}
												},
												"children": []
											}
										]
									}
								]
							},
							{
								"objectId": "BCF4AA7F-18F9-48FF-83C8-EAEE2D36CF13",
								"kind": "group",
								"name": "add_emoji_button",
								"originalName": "add-emoji-button",
								"maskFrame": null,
								"layerFrame": {
									"x": 129,
									"y": 654,
									"width": 61,
									"height": 32
								},
								"visible": true,
								"metadata": {
									"opacity": 1
								},
								"image": {
									"path": "images/Layer-add_emoji_button-qkngnefb.png",
									"frame": {
										"x": 129,
										"y": 654,
										"width": 61,
										"height": 32
									}
								},
								"children": []
							}
						]
					},
					{
						"objectId": "34351B28-13EB-44D2-9D86-53DF835A8288",
						"kind": "group",
						"name": "description",
						"originalName": "description",
						"maskFrame": null,
						"layerFrame": {
							"x": 12,
							"y": 63,
							"width": 972,
							"height": 523
						},
						"visible": true,
						"metadata": {
							"opacity": 1
						},
						"image": {
							"path": "images/Layer-description-mzqzntfc.jpg",
							"frame": {
								"x": 12,
								"y": 63,
								"width": 972,
								"height": 523
							}
						},
						"children": []
					}
				]
			}
		]
	}
]